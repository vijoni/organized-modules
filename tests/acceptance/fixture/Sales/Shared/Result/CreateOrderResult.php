<?php

namespace VijoniTest\Acceptance\Fixture\Sales\Shared\Result;

class CreateOrderResult
{
  public function __construct(private bool $isSuccess)
  {
  }

  public function isSuccess(): bool
  {
    return $this->isSuccess;
  }
}
