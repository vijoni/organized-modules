<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Shared;

class Order
{
  public function __construct(private array $properties)
  {
  }

  public function getBid(): string
  {
    return 'random-business-id';
  }

  public function toArray(): array
  {
    return $this->properties;
  }
}
