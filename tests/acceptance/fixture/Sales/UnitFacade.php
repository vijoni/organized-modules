<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales;

use Vijoni\Unit\BaseUnitFacade;

class UnitFacade extends BaseUnitFacade
{
}
