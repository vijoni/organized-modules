<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Payment;

use Vijoni\Unit\BaseModuleConfig;

class ModuleConfig extends BaseModuleConfig
{
  public function readStripeApiKey(): string
  {
    return $this->config()->getString('stripe.api_key');
  }
}
