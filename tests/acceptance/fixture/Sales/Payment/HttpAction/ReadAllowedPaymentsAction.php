<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Payment\HttpAction;

use Vijoni\Unit\ModuleAction;
use VijoniTest\Acceptance\Fixture\Sales\Order\ModuleFactory;

/**
 * @method ModuleFactory moduleFactory()
 */
class ReadAllowedPaymentsAction extends ModuleAction
{
}
