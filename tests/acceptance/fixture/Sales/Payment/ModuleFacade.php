<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Payment;

use Vijoni\Unit\BaseModuleFacade;

class ModuleFacade extends BaseModuleFacade
{
  public function dummyBoolValue(): bool
  {
    return true;
  }
}
