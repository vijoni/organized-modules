<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Payment;

use Vijoni\Unit\BaseModuleFactory;

/**
 * @method ModuleDependencyProvider dependencyProvider()
 * @method ModuleConfig config()
 */
class ModuleFactory extends BaseModuleFactory
{
}
