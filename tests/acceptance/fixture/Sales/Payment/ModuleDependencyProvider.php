<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Payment;

use Vijoni\Unit\BaseModuleDependencyProvider;

class ModuleDependencyProvider extends BaseModuleDependencyProvider
{
}
