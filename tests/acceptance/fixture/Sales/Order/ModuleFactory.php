<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Order;

use Vijoni\Unit\BaseModuleConfig;
use Vijoni\Unit\BaseModuleFactory;
use VijoniTest\Acceptance\Fixture\Sales\Order\Gateway\OrderReadGateway;
use VijoniTest\Acceptance\Fixture\Sales\Order\Gateway\OrderWriteGateway;
use VijoniTest\Acceptance\Fixture\Sales\Order\Repository\OrderRepository;
use VijoniTest\Acceptance\Fixture\Sales\Order\Service\CreateOrderService;
use VijoniTest\Acceptance\Fixture\Sales\Order\Validator\OrderValidator;

/**
 * @method ModuleDependencyProvider dependencyProvider()
 * @method ModuleConfig config()
 */
class ModuleFactory extends BaseModuleFactory
{
  public function newCreateOrderService(): CreateOrderService
  {
    return new CreateOrderService($this->shareOrderRepository(), $this->shareOrderValidator());
  }

  protected function shareOrderRepository(): OrderRepository
  {
    /** @var OrderRepository */
    return $this->share(
      OrderRepository::class,
      fn () => new OrderRepository($this->shareOrderReadGateway(), $this->shareOrderWriteGateway())
    );
  }

  protected function shareOrderReadGateway(): OrderReadGateway
  {
    /** @var OrderReadGateway */
    return $this->share(
      OrderReadGateway::class,
      fn () => new OrderReadGateway()
    );
  }

  protected function shareOrderWriteGateway(): OrderWriteGateway
  {
    /** @var OrderWriteGateway */
    return $this->share(
      OrderWriteGateway::class,
      fn () => new OrderWriteGateway()
    );
  }

  protected function shareOrderValidator(): OrderValidator
  {
    $paymentFacade = $this->dependencyProvider()->sharePaymentFacade();

    /** @var OrderValidator */
    return $this->share(
      OrderValidator::class,
      fn () => new OrderValidator($paymentFacade)
    );
  }
}
