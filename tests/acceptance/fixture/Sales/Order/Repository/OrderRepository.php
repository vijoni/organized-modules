<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Order\Repository;

use VijoniTest\Acceptance\Fixture\Sales\Order\Gateway\OrderReadGateway;
use VijoniTest\Acceptance\Fixture\Sales\Order\Gateway\OrderWriteGateway;
use VijoniTest\Acceptance\Fixture\Sales\Shared\Order;

class OrderRepository
{
  public function __construct(
    private OrderReadGateway $readGateway,
    private OrderWriteGateway $writeGateway
  ) {
  }

  public function readOrderByBid(string $orderBid): Order
  {
    $orderProperties = $this->readGateway->readOrderById($orderBid);

    // map the result to domain object

    return new Order($orderProperties);
  }

  public function saveOrder(Order $order): void
  {
    $orderProperties = $order->toArray();
    $this->writeGateway->saveOrder($order->getBid(), $orderProperties);
  }
}
