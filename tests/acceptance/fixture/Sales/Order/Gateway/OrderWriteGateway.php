<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Order\Gateway;

class OrderWriteGateway
{
  public function saveOrder(string $orderBid, array $orderProperties): void
  {
    // communicate with storage
  }
}
