<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Order;

use Vijoni\Unit\BaseModuleConfig;

class ModuleConfig extends BaseModuleConfig
{
}
