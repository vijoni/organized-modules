<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Order;

use Vijoni\Unit\BaseModuleDependencyProvider;
use VijoniTest\Acceptance\Fixture\Sales\Payment\ModuleFacade as PaymentModuleFacade;

class ModuleDependencyProvider extends BaseModuleDependencyProvider
{
  public function sharePaymentFacade(): PaymentModuleFacade
  {
    /** @var PaymentModuleFacade */
    return $this->dependencyProvider()->shareModuleFacade(PaymentModuleFacade::class);
  }
}
