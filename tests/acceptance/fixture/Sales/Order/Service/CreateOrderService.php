<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Order\Service;

use VijoniTest\Acceptance\Fixture\Sales\Order\Repository\OrderRepository;
use VijoniTest\Acceptance\Fixture\Sales\Order\Validator\OrderValidator;
use VijoniTest\Acceptance\Fixture\Sales\Shared\Order;
use VijoniTest\Acceptance\Fixture\Sales\Shared\Result\CreateOrderResult;

class CreateOrderService
{
  public function __construct(
    private OrderRepository $orderRepository,
    private OrderValidator $orderValidator
  ) {
  }

  public function createOrder(Order $order): CreateOrderResult
  {
    $isOrderValid = $this->orderValidator->validateOrder($order);
    if ($isOrderValid) {
      $this->orderRepository->saveOrder($order);
      return new CreateOrderResult(true);
    }

    return new CreateOrderResult(false);
  }
}
