<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Order;

use Vijoni\Unit\BaseModuleFacade;

class ModuleFacade extends BaseModuleFacade
{
}
