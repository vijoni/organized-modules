<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Order\HttpAction;

use Vijoni\Unit\DependencyProvider;
use Vijoni\Unit\ModuleActionDependency;
use Vijoni\Unit\ModuleActionInterface;
use VijoniTest\Acceptance\Fixture\Sales\Order\ModuleFactory;

/**
 * @method ModuleFactory moduleFactory()
 */
class ValidateOrderAction implements ModuleActionInterface
{
  use ModuleActionDependency;

  public function __construct()
  {
    DependencyProvider::getInstance()->fillActionDependencies($this);
  }
}
