<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Order\HttpAction;

use Vijoni\Unit\ModuleAction;
use VijoniTest\Acceptance\Fixture\Sales\Order\ModuleFactory;

/**
 * @method ModuleFactory moduleFactory()
 */
class ReadOrderAction extends ModuleAction
{
}
