<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Order\HttpAction;

use Vijoni\Unit\DependencyProvider;
use Vijoni\Unit\ModuleActionInterface;
use Vijoni\Unit\ModuleActionDependency;
use VijoniTest\Acceptance\Fixture\Sales\Order\ModuleFactory;

/**
 * @method ModuleFactory moduleFactory()
 */
class CreateOrderAction implements ModuleActionInterface
{
  use ModuleActionDependency;

  public function __construct()
  {
    DependencyProvider::getInstance()->fillActionDependencies($this);
  }

  public function __invoke(): void
  {
    $orderService = $this->moduleFactory()->newCreateOrderService();
  }
}
