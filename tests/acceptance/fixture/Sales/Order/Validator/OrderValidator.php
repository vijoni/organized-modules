<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Fixture\Sales\Order\Validator;

use VijoniTest\Acceptance\Fixture\Sales\Payment\ModuleFacade as PaymentModuleFacade;
use VijoniTest\Acceptance\Fixture\Sales\Shared\Order;

class OrderValidator
{
  public function __construct(private PaymentModuleFacade $paymentModuleFacade)
  {
  }

  public function validateOrder(Order $order): bool
  {
    return $this->paymentModuleFacade->dummyBoolValue();
  }
}
