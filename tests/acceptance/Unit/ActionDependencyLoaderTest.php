<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Unit;

use ReflectionClass;
use Vijoni\Unit\DependencyProvider;
use VijoniTest\Acceptance\Fixture\Sales\Order\HttpAction\CreateOrderAction;
use VijoniTest\Acceptance\Fixture\Sales\Order\HttpAction\ReadOrderAction;
use VijoniTest\Acceptance\Fixture\Sales\Order\ModuleConfig;
use VijoniTest\Acceptance\Fixture\Sales\Order\ModuleConfigDE;
use VijoniTest\Acceptance\Fixture\Sales\Order\ModuleFactory;
use VijoniTest\Acceptance\Fixture\Sales\Order\ModuleFactoryDE;

class ActionDependencyLoaderTest extends \Codeception\Test\Unit
{
  public function testDiscoverDependenciesUsingTrait(): void
  {
    $action = new CreateOrderAction();

    $moduleFactory = $action->moduleFactory();
    $this->assertInstanceOf(ModuleFactory::class, $moduleFactory);

    $moduleFactoryReflectionClass = new ReflectionClass(get_class($moduleFactory));
    $moduleConfig = $moduleFactoryReflectionClass->getMethod('config')->invoke($moduleFactory);
    $this->assertInstanceOf(ModuleConfig::class, $moduleConfig);
  }

  public function testDiscoverDependenciesUsingExtension(): void
  {
    $action = new ReadOrderAction();

    $moduleFactory = $action->moduleFactory();
    $this->assertInstanceOf(ModuleFactory::class, $moduleFactory);

    $moduleFactoryReflectionClass = new ReflectionClass(get_class($moduleFactory));
    $moduleConfig = $moduleFactoryReflectionClass->getMethod('config')->invoke($moduleFactory);
    $this->assertInstanceOf(ModuleConfig::class, $moduleConfig);
  }

  public function testDiscoverDependenciesGivenApplicationVariant(): void
  {
    $dp = DependencyProvider::getInstance();
    $dp->setVariant('DE');
    $action = new CreateOrderAction();
    $dp->setVariant('');

    $moduleFactory = $action->moduleFactory();
    $this->assertInstanceOf(ModuleFactoryDE::class, $moduleFactory);

    $moduleFactoryReflectionClass = new ReflectionClass(get_class($moduleFactory));
    $moduleConfig = $moduleFactoryReflectionClass->getMethod('config')->invoke($moduleFactory);
    $this->assertInstanceOf(ModuleConfigDE::class, $moduleConfig);
  }
}
