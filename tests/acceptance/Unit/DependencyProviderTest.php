<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Unit;

use ReflectionClass;
use Vijoni\Unit\DependencyProvider;
use VijoniTest\Acceptance\Fixture\Sales\Payment\ModuleFacade as PaymentModuleFacade;
use VijoniTest\Acceptance\Fixture\Sales\Payment\ModuleFactory as PaymentModuleFactory;
use VijoniTest\Acceptance\Fixture\Sales\UnitFacade as SalesUnitFacade;

class DependencyProviderTest extends \Codeception\Test\Unit
{
  public function testObjectSharing(): void
  {
    $tmpClass = new class {
    };

    $dp = DependencyProvider::getInstance();
    $dp->register($tmpClass::class, fn () => new $tmpClass());

    $instanceA = $dp->share($tmpClass::class);
    $instanceB = $dp->share($tmpClass::class);

    $this->assertSame($instanceA, $instanceB);
  }

  public function testModuleFacadeLoading(): void
  {
    $dp = DependencyProvider::getInstance();
    /** @var PaymentModuleFacade $moduleFacade */
    $moduleFacade = $dp->shareModuleFacade(PaymentModuleFacade::class);
    $moduleFactory = $moduleFacade->moduleFactory();

    $this->assertInstanceOf(PaymentModuleFacade::class, $moduleFacade);
    $this->assertInstanceOf(PaymentModuleFactory::class, $moduleFactory);
  }

  public function testModuleFacadeSharing(): void
  {
    $dp = DependencyProvider::getInstance();
    $moduleFacadeA = $dp->shareModuleFacade(PaymentModuleFacade::class);
    $moduleFacadeB = $dp->shareModuleFacade(PaymentModuleFacade::class);

    $this->assertSame($moduleFacadeA, $moduleFacadeB);
  }

  public function testModuleFacadeFactorySharing(): void
  {
    $dp = DependencyProvider::getInstance();
    /** @var PaymentModuleFacade $moduleFacadeA */
    $moduleFacadeA = $dp->shareModuleFacade(PaymentModuleFacade::class);
    /** @var PaymentModuleFacade $moduleFacadeB */
    $moduleFacadeB = $dp->shareModuleFacade(PaymentModuleFacade::class);

    $moduleFactoryA = $moduleFacadeA->moduleFactory();
    $moduleFactoryB = $moduleFacadeB->moduleFactory();

    $this->assertSame($moduleFactoryA, $moduleFactoryB);
  }

  public function testUnitFacadeLoading(): void
  {
    $dp = DependencyProvider::getInstance();
    /** @var SalesUnitFacade $unitFacade */
    $unitFacade = $dp->shareUnitFacade(SalesUnitFacade::class);

    $unitFacadeReflectionClass = new ReflectionClass(get_class($unitFacade));
    $dependencyProvider = $unitFacadeReflectionClass->getMethod('dependencyProvider')->invoke($unitFacade);

    $this->assertSame($dp, $dependencyProvider);
  }

  public function testUnitFacadeSharing(): void
  {
    $dp = DependencyProvider::getInstance();
    $unitFacadeA = $dp->shareUnitFacade(SalesUnitFacade::class);
    $unitFacadeB = $dp->shareUnitFacade(SalesUnitFacade::class);

    $this->assertSame($unitFacadeA, $unitFacadeB);
  }

  public function testInstanceCreation(): void
  {
    $tmpClass = new class {
    };
    $randomDependencyKey = 'random_key';

    $dp = DependencyProvider::getInstance();
    $dp->register($randomDependencyKey, fn () => new $tmpClass());

    $instanceA = $dp->create($randomDependencyKey);
    $instanceB = $dp->create($randomDependencyKey);

    $this->assertNotSame($instanceA, $instanceB);
  }

  public function testDefaultConfig(): void
  {
    //config is loaded in test bootstrap file
    $dp = DependencyProvider::getInstance();
    $value = $dp->getConfig()->getString('database.host');

    $this->assertSame('127.0.0.1', $value);
  }
}
