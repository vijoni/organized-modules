<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Unit;

use ReflectionClass;
use Vijoni\Unit\DependencyProvider;
use VijoniTest\Acceptance\Fixture\Sales\Order\ModuleConfig;
use VijoniTest\Acceptance\Fixture\Sales\Order\ModuleDependencyProvider;
use VijoniTest\Acceptance\Fixture\Sales\Order\ModuleFactory;

class ModuleFactoryTest extends \Codeception\Test\Unit
{
  public function testUniqueObjectInstances(): void
  {
    $moduleConfigMock = $this->createMock(ModuleConfig::class);
    $dependencyProvider = new ModuleDependencyProvider(DependencyProvider::getInstance());
    $moduleFactory = new ModuleFactory($dependencyProvider, $moduleConfigMock);

    $createOrderServiceInstanceA = $moduleFactory->newCreateOrderService();
    $createOrderServiceInstanceB = $moduleFactory->newCreateOrderService();

    $this->assertNotSame($createOrderServiceInstanceA, $createOrderServiceInstanceB);
  }

  public function testSharedObjectInstances(): void
  {
    $moduleConfigMock = $this->createMock(ModuleConfig::class);
    $dependencyProvider = new ModuleDependencyProvider(DependencyProvider::getInstance());
    $moduleFactory = new ModuleFactory($dependencyProvider, $moduleConfigMock);

    $moduleFactoryReflectionClass = new ReflectionClass(get_class($moduleFactory));
    $sharedMethod = $moduleFactoryReflectionClass->getMethod('shareOrderValidator');

    $orderValidatorInstanceA = $sharedMethod->invoke($moduleFactory);
    $orderValidatorInstanceB = $sharedMethod->invoke($moduleFactory);

    $this->assertSame($orderValidatorInstanceA, $orderValidatorInstanceB);
  }
}
