<?php

return [
  'database' => [
    'host' => '127.0.0.1',
    'user' => 'dbuser',
  ],
  'stripe' => [
    'api_key' => 'xxx-xxx-xxx',
  ]
];
