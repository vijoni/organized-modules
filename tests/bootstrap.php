<?php

declare(strict_types=1);

use Vijoni\Unit\AppConfig;
use Vijoni\Unit\DependencyProvider;

require_once __DIR__ . '/../src/bootstrap.php';

$appConfig = AppConfig::fromFile(__DIR__ . '/app_config.php');
$dependencyProvider = DependencyProvider::getInstance();
$dependencyProvider->setConfig($appConfig);
$dependencyProvider->setVariant('');
