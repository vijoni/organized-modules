<?php

declare(strict_types=1);

namespace VijoniTest\Unit\Unit;

use Vijoni\Unit\AppConfig;
use Vijoni\Unit\Exception\ConfigValueException;

class AppConfigTest extends \Codeception\Test\Unit
{
  public function testReadConfig(): void
  {
  }

  public function testReadConfigStringValue(): void
  {
    $config = [
      'api_key' => 'xxx-xxx-xxx',
    ];
    $appConfig = new AppConfig($config);

    $configValue = $appConfig->getString('api_key');
    $this->assertSame('xxx-xxx-xxx', $configValue);
  }

  public function testReadConfigIntValue(): void
  {
    $config = [
      'age' => 21,
    ];
    $appConfig = new AppConfig($config);

    $configValue = $appConfig->getInt('age');
    $this->assertSame(21, $configValue);
  }

  public function testReadConfigIntValueGivenString(): void
  {
    $config = [
      'age' => 'xxx',
    ];
    $appConfig = new AppConfig($config);

    $this->expectException(ConfigValueException::class);
    $appConfig->getInt('age');
  }

  public function testReadConfigFloatValue(): void
  {
    $config = [
      'price' => 3.23,
    ];
    $appConfig = new AppConfig($config);

    $configValue = $appConfig->getFloat('price');
    $this->assertSame(3.23, $configValue);
  }

  public function testReadConfigFloatValueGivenString(): void
  {
    $config = [
      'price' => 'xxx',
    ];
    $appConfig = new AppConfig($config);

    $this->expectException(ConfigValueException::class);
    $appConfig->getFloat('price');
  }

  public function testReadConfigBooleanValue(): void
  {
    $config = [
      'is_enabled' => true,
    ];
    $appConfig = new AppConfig($config);

    $configValue = $appConfig->getBoolean('is_enabled');
    $this->assertSame(true, $configValue);
  }

  public function testReadConfigArrayValue(): void
  {
    $config = [
      'allowed_payments' => [
        'credit card',
        'sepa',
      ],
    ];
    $appConfig = new AppConfig($config);

    $configValue = $appConfig->getArray('allowed_payments');
    $this->assertSame(['credit card', 'sepa'], $configValue);
  }

  public function testReadConfigArrayValueGivenString(): void
  {
    $config = [
      'allowed_payments' => 'credit card, sepa',
    ];
    $appConfig = new AppConfig($config);

    $this->expectException(ConfigValueException::class);
    $appConfig->getArray('allowed_payments');
  }

  public function testExtractConfig(): void
  {
    $config = [
      'payment' => [
        'stripe' => [
          'api_key' => 'xxx-xxx-xxx',
        ],
      ],
    ];
    $appConfig = new AppConfig($config);

    $extractedConfig = $appConfig->extractConfig('payment');
    $value = $extractedConfig->getString('stripe.api_key');
    $this->assertSame('xxx-xxx-xxx', $value);
  }

  public function testExtractConfigUniqueness(): void
  {
    $config = [
      'payment' => [
        'stripe' => [
          'api_key' => 'xxx-xxx-xxx',
        ],
      ],
    ];
    $appConfig = new AppConfig($config);

    $extractedConfig = $appConfig->extractConfig('payment');
    $this->assertNotSame($appConfig, $extractedConfig);

    $value = $appConfig->getString('payment.stripe.api_key');
    $extractedValue = $extractedConfig->getString('stripe.api_key');
    $this->assertSame($value, $extractedValue);
  }

  public function testReadConfigByPath(): void
  {
    $config = [
      'payment' => [
        'stripe' => [
          'api_key' => 'xxx-xxx-xxx',
        ],
      ],
    ];
    $appConfig = new AppConfig($config);

    $apiKey = $appConfig->get('payment.stripe.api_key');
    $this->assertSame('xxx-xxx-xxx', $apiKey);
  }
}
