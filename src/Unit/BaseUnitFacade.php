<?php

declare(strict_types=1);

namespace Vijoni\Unit;

class BaseUnitFacade
{
  public function __construct(private DependencyProvider $dependencyProvider)
  {
  }

  protected function dependencyProvider(): DependencyProvider
  {
    return $this->dependencyProvider;
  }
}
