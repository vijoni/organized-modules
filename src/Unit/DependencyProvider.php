<?php

declare(strict_types=1);

namespace Vijoni\Unit;

class DependencyProvider
{
  private array $instanceCache = [];

  private array $creationRegistry = [];

  private static DependencyProvider|null $instance = null;

  private string $variant = '';

  private AppConfig $config;

  private function __construct()
  {
  }

  public static function getInstance(): self
  {
    if (self::$instance === null) {
      self::$instance = new DependencyProvider();
      self::$instance->setConfig(new AppConfig([]));
    }

    return self::$instance;
  }

  public function setVariant(string $variant): void
  {
    $this->variant = $variant;
  }

  public function setConfig(AppConfig $config): void
  {
    $this->config = $config;
  }

  public function getConfig(): AppConfig
  {
    return $this->config;
  }

  public function define(string $key, mixed $instance): void
  {
    if (!isset($this->instanceCache[$key])) {
      $this->instanceCache[$key] = $instance;
    }
  }

  public function register(string $key, \Closure $createCallback): void
  {
    if (!isset($this->creationRegistry[$key])) {
      $this->creationRegistry[$key] = $createCallback;
    }
  }

  public function share(string $key): mixed
  {
    if (!isset($this->instanceCache[$key])) {
      $this->define($key, $this->creationRegistry[$key]());
    }

    return $this->instanceCache[$key];
  }

  public function create(string $key): mixed
  {
    return $this->creationRegistry[$key]();
  }

  public function shareModuleFacade(string $facadeFullClassName): BaseModuleFacade
  {
    if (!isset($this->instanceCache[$facadeFullClassName])) {
      $moduleNamespace = $this->discoverModuleNamespaceByFacade($facadeFullClassName);
      $moduleFactoryClassName = self::readModuleClassName($moduleNamespace, 'ModuleFactory');
      $moduleFactory = $this->shareModuleFactory($moduleFactoryClassName);
      $moduleFacade = new $facadeFullClassName($moduleFactory);
      $this->define($facadeFullClassName, $moduleFacade);
    }

    return $this->instanceCache[$facadeFullClassName];
  }

  public function shareUnitFacade(string $facadeFullClassName): BaseUnitFacade
  {
    if (!isset($this->instanceCache[$facadeFullClassName])) {
      $unitFacade = new $facadeFullClassName($this);
      $this->define($facadeFullClassName, $unitFacade);
    }

    return $this->instanceCache[$facadeFullClassName];
  }

  private function shareModuleFactory(string $factoryFullClassName): BaseModuleFactory
  {
    if (!isset($this->instanceCache[$factoryFullClassName])) {
      $moduleNamespace = $this->discoverModuleNamespaceByFactory($factoryFullClassName);

      $moduleFactoryClassName = self::readModuleClassName($moduleNamespace, 'ModuleFactory');
      $moduleConfigClassname = self::readModuleClassName($moduleNamespace, 'ModuleConfig');
      $moduleDependencyProviderClassname = self::readModuleClassName($moduleNamespace, 'ModuleDependencyProvider');

      $moduleConfig = new $moduleConfigClassname($this->getConfig());
      $moduleDependencyProvider = new $moduleDependencyProviderClassname($this);
      $moduleFactory = new $moduleFactoryClassName($moduleDependencyProvider, $moduleConfig);

      $this->define($factoryFullClassName, $moduleFactory);
    }

    return $this->instanceCache[$factoryFullClassName];
  }

  public function fillActionDependencies(ModuleActionInterface $moduleAction): void
  {
    $moduleNamespace = $this->discoverModuleNamespaceByAction($moduleAction::class);
    $moduleFactoryClassName = self::readModuleClassName($moduleNamespace, 'ModuleFactory');

    $moduleFactory = $this->shareModuleFactory($moduleFactoryClassName);
    $this->define($moduleFactory::class, $moduleFactory);
    $moduleAction->setModuleFactory($moduleFactory);
  }

  private function discoverModuleNamespaceByFactory(string $factoryFullClassName): string
  {
    return $this->discoverModuleNamespace($factoryFullClassName, 1);
  }

  private function discoverModuleNamespaceByFacade(string $facadeFullClassName): string
  {
    return $this->discoverModuleNamespace($facadeFullClassName, 1);
  }

  private function discoverModuleNamespaceByAction(string $actionFullClassName): string
  {
    return $this->discoverModuleNamespace($actionFullClassName, 2);
  }

  private function discoverModuleNamespace(string $fullClassName, int $nestLevel): string
  {
    $namespaceTokens = explode('\\', $fullClassName);
    $moduleNamespaceTokens = array_slice($namespaceTokens, 0, $nestLevel * -1);

    return implode('\\', $moduleNamespaceTokens);
  }

  private function readModuleClassName(string $moduleNamespace, string $className): string
  {
    $moduleClassName = $moduleNamespace . '\\' . $className;
    $moduleVariantClassName = $moduleClassName . $this->variant;

    return class_exists($moduleVariantClassName) ? $moduleVariantClassName : $moduleClassName;
  }
}
