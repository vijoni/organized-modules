<?php

declare(strict_types=1);

namespace Vijoni\Unit;

class BaseModuleFacade
{
  public function __construct(private BaseModuleFactory $moduleFactory)
  {
  }

  public function moduleFactory(): BaseModuleFactory
  {
    return $this->moduleFactory;
  }
}
