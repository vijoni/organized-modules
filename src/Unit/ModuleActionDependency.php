<?php

namespace Vijoni\Unit;

trait ModuleActionDependency
{
  protected BaseModuleFactory $moduleFactory;

  public function setModuleFactory(BaseModuleFactory $moduleFactory): void
  {
    $this->moduleFactory = $moduleFactory;
  }

  public function moduleFactory(): BaseModuleFactory
  {
    return $this->moduleFactory;
  }
}
