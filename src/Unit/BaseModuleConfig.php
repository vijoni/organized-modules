<?php

declare(strict_types=1);

namespace Vijoni\Unit;

class BaseModuleConfig
{
  public function __construct(private AppConfig $config)
  {
  }

  protected function config(): AppConfig
  {
    return $this->config;
  }
}
