<?php

namespace Vijoni\Unit;

interface ModuleActionInterface
{
  public function setModuleFactory(BaseModuleFactory $moduleFactory): void;

  public function moduleFactory(): BaseModuleFactory;
}
