<?php

declare(strict_types=1);

namespace Vijoni\Unit;

use Vijoni\Unit\Exception\ConfigValueException;

class AppConfig
{
  public function __construct(private array $config)
  {
  }

  public static function fromFile(string $filePath): self
  {
    $config = require_once $filePath;

    return new AppConfig($config);
  }

  public function getString(string $path): string
  {
    $value = $this->get($path);
    if (is_array($value)) {
      throw new ConfigValueException("Config value could not be converted to string, config path: [{$path}]");
    }

    return strval($value);
  }

  public function getInt(string $path): int
  {
    $value = $this->get($path);
    $value = filter_var($value, FILTER_VALIDATE_INT);
    if ($value === false) {
      throw new ConfigValueException("Config value could not be converted to integer: [{$path}]");
    }

    return $value;
  }

  public function getFloat(string $path): float
  {
    $value = $this->get($path);
    $value = filter_var($value, FILTER_VALIDATE_FLOAT);
    if ($value === false) {
      throw new ConfigValueException("Config value could not be converted to float: [{$path}]");
    }

    return $value;
  }

  public function getBoolean(string $path): bool
  {
    $value = $this->get($path);
    $value = filter_var($value, FILTER_VALIDATE_BOOL);
    if ($value === false) {
      throw new ConfigValueException("Config value could not be converted to boolean, config path: [{$path}]");
    }

    return $value;
  }

  public function getArray(string $path): array
  {
    $value = $this->get($path);
    if (!is_array($value)) {
      throw new ConfigValueException("Read config value is not an array, config path: [{$path}]");
    }

    return $value;
  }

  public function extractConfig(string $path): self
  {
    try {
      $value = $this->getArray($path);
    } catch (ConfigValueException $e) {
      throw new ConfigValueException(
        "Read config value is not an array. Only arrays can be converted to Config instance, config path: [{$path}]"
      );
    }

    return new AppConfig($value);
  }

  public function get(string $path): mixed
  {
    $key = strtok($path, '.');
    $value = $this->config;

    while ($key !== false) {
      $value = $value[$key];
      $key = strtok('.');
    }

    return $value;
  }
}
