<?php

declare(strict_types=1);

namespace Vijoni\Unit\Exception;

class ConfigValueException extends \Exception
{
}
