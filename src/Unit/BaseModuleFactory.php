<?php

declare(strict_types=1);

namespace Vijoni\Unit;

class BaseModuleFactory
{
  private array $instanceCache = [];

  public function __construct(
    private BaseModuleDependencyProvider $dependencyProvider,
    private BaseModuleConfig $moduleConfig
  ) {
  }

  protected function dependencyProvider(): BaseModuleDependencyProvider
  {
    return $this->dependencyProvider;
  }

  protected function config(): BaseModuleConfig
  {
    return $this->moduleConfig;
  }

  protected function share(string $instanceKey, \Closure $createCallback): mixed
  {
    if (!isset($this->instanceCache[$instanceKey])) {
      $this->instanceCache[$instanceKey] = $createCallback();
    }

    return $this->instanceCache[$instanceKey];
  }
}
