<?php

declare(strict_types=1);

namespace Vijoni\Unit;

class BaseModuleDependencyProvider
{
  public function __construct(private DependencyProvider $dependencyProvider)
  {
  }

  protected function dependencyProvider(): DependencyProvider
  {
    return $this->dependencyProvider;
  }
}
